// a generic missing handler
const missingHandler = () =>
  new Response("404, not found!", { status: 404 })

export default missingHandler
