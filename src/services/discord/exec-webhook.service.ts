import { ExecuteWebhook } from '../../models/discord';
import { ExecuteWebhookProps } from '../../models/discord/execute-webhook.model';

const WEBHOOK_DISCORD = 'https://discord.com/api/webhooks'

export const execWebhook = async (body: ExecuteWebhook, props: ExecuteWebhookProps) => {
  const url = `${WEBHOOK_DISCORD}/${props.id}/${props.token}`

  const params: RequestInit = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify(body)
  }

  return await fetch(url, params)
}
