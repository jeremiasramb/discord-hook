// a generic error handler
const errorHandler = (error: any) =>
  new Response(error.message || 'Server Error', { status: error.status || 500 })

export default errorHandler
