import { Router, IHTTPMethods } from 'itty-router'
import errorHandler from './services/error-handler.service'
import missingHandler from './services/missing-handler.service'
import GitlabWebhookRouter from './api/gitlab-webhook.router'
import { API_ROOTPATH } from './models/constants'
import { IttyRequest } from './models/request.model'

// Create a new router
const router = Router<IttyRequest, IHTTPMethods>()

router
  .get('/', () => new Response('Hello world'))
  .all(`/${API_ROOTPATH}/*`, GitlabWebhookRouter.handle)
  .all('*', missingHandler)

/** The entrypoint to handle all fetch request */
addEventListener('fetch', event => {
  event.respondWith(
    router
      .handle(event.request)
      .catch(errorHandler)
  )
})
