
export type MERGE_HOOKS_TYPES = 
  'GITLAB_MERGE_REQUEST_DISCORD_HOOK'
  | 'TEST'

export interface Env {
  // Example binding to KV. Learn more at https://developers.cloudflare.com/workers/runtime-apis/kv/
  // MERGE_HOOKS: KVNamespace<MERGE_HOOKS_TYPES>
  //
  // Example binding to Durable Object. Learn more at https://developers.cloudflare.com/workers/runtime-apis/durable-objects/
  // MY_DURABLE_OBJECT: DurableObjectNamespace;
  //
  // Example binding to R2. Learn more at https://developers.cloudflare.com/workers/runtime-apis/r2/
  // MY_BUCKET: R2Bucket;
}

declare global {
  const MERGE_HOOKS: KVNamespace<MERGE_HOOKS_TYPES>
}
