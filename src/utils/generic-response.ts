const DEFAULT_BODY = {
  status: 'ok'
}

const DEFAULT_STATUS_CODE = 200

export const generateResponse = (statusCode: number = DEFAULT_STATUS_CODE, body: any = DEFAULT_BODY) =>
  new Response(
    JSON.stringify(body),
    {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      },
      status: statusCode
    }
  )
