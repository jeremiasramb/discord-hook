import { Router, IHTTPMethods } from 'itty-router'
import { API_ROOTPATH } from '../models/constants'
import { IttyRequest } from '../models/request.model'
import { handleMergeRequest } from './gitlab-webhook.handler'

const GitlabWebhookRouter = Router<IttyRequest, IHTTPMethods>({
  base: `/${API_ROOTPATH}/gitlab`
})

GitlabWebhookRouter
  .get('/', () => new Response('Gitlab webhooks'))
  .post('/:project', handleMergeRequest)

export default GitlabWebhookRouter
