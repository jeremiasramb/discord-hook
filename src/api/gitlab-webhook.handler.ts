import { TOKEN_HEADER, MergeRequestEvent, EVENT_HEADER } from '../models/gitlab'
import { GitlabMergeRequestDiscordHook } from '../models/kv'
import { IttyRequest } from '../models/request.model'
import { execWebhook } from '../services/discord/exec-webhook.service'
import { generateResponse } from '../utils/generic-response'

export const handleMergeRequest = async (request: IttyRequest) => {
  const settings = await MERGE_HOOKS.get<GitlabMergeRequestDiscordHook[]>('GITLAB_MERGE_REQUEST_DISCORD_HOOK', 'json')
  const project = request.params?.project
  const body: MergeRequestEvent = await request.json?.()
  const projectSettings = settings?.find(x => x.project === project)

  if (request.headers.get(EVENT_HEADER) !== 'Merge Request Hook')
    throw new Error('Invalid Event')

  if (body.object_attributes.action !== 'merge')
    throw new Error('Invalid Merge Event')

  if (!projectSettings)
    throw new Error('Invalid Project')

  if (request.headers.get(TOKEN_HEADER) !== projectSettings.gitlabToken)
    throw new Error('Invalid Token')

  await execWebhook({
    embeds: [
      {
        title: `Nuevo merge en ${projectSettings.projectName}`,
        description: body.object_attributes.title,
        url: body.object_attributes.url,
        color: 5814783,
        author: {
          name: body.user.name,
          url: `https://gitlab.com/${body.user.username}`,
          icon_url: body.user.avatar_url
        },
        fields: [
          {
            name: 'Descripción',
            value: body.object_attributes.description
          }
        ],
        footer: {
          text: `${body.object_attributes.source_branch} → ${body.object_attributes.target_branch}`,
          icon_url: 'https://gitlab.com/gitlab-org/gitlab/-/raw/master/public/slash-command-logo.png'
        },
        timestamp: new Date().toISOString(),
        thumbnail: {
          url: 'https://gitlab.com/uploads/-/system/group/avatar/9970/project_avatar.png?width=64'
        }
      }
    ]
  }, {
    id: projectSettings.channelId,
    token: projectSettings.discordToken
  })

  return generateResponse()
}
