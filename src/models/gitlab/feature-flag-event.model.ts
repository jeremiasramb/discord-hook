// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#feature-flag-events

// X-Gitlab-Event: Feature Flag Hook

export interface FeatureFlagEvent {
  object_kind:       string;
  project:           Project;
  user:              User;
  user_url:          string;
  object_attributes: ObjectAttributes;
}

interface ObjectAttributes {
  id:          number;
  name:        string;
  description: string;
  active:      boolean;
}

interface Project {
  id:                  number;
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
  ci_config_path:      null;
  homepage:            string;
  url:                 string;
  ssh_url:             string;
  http_url:            string;
}

interface User {
  id:         number;
  name:       string;
  username:   string;
  avatar_url: string;
  email:      string;
}
