// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#tag-events

// X-Gitlab-Event: Tag Push Hook

export interface TagEvent {
  object_kind:         string;
  event_name:          string;
  before:              string;
  after:               string;
  ref:                 string;
  checkout_sha:        string;
  user_id:             number;
  user_name:           string;
  user_avatar:         string;
  project_id:          number;
  project:             Project;
  repository:          Repository;
  commits:             any[];
  total_commits_count: number;
}

interface Project {
  id:                  number;
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
  homepage:            string;
  url:                 string;
  ssh_url:             string;
  http_url:            string;
}

interface Repository {
  name:             string;
  url:              string;
  description:      string;
  homepage:         string;
  git_http_url:     string;
  git_ssh_url:      string;
  visibility_level: number;
}
