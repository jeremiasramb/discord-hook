// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#comment-events

// X-Gitlab-Event: Note Hook

export interface NoteEvent {
  object_kind:       string;
  event_type:        string;
  user:              User;
  project_id:        number;
  project:           Project;
  repository:        Repository;
  object_attributes: ObjectAttributes;
  commit:            Commit;
}

interface Commit {
  id:        string;
  message:   string;
  timestamp: string;
  url:       string;
  author:    Author;
}

interface Author {
  name:  string;
  email: string;
}

interface ObjectAttributes {
  id:            number;
  note:          string;
  noteable_type: string;
  author_id:     number;
  created_at:    string;
  updated_at:    string;
  project_id:    number;
  attachment:    null;
  line_code:     string;
  commit_id:     string;
  noteable_id:   null;
  system:        boolean;
  st_diff:       StDiff;
  url:           string;
}

interface StDiff {
  diff:         string;
  new_path:     string;
  old_path:     string;
  a_mode:       string;
  b_mode:       string;
  new_file:     boolean;
  renamed_file: boolean;
  deleted_file: boolean;
}

interface Project {
  id:                  number;
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
  homepage:            string;
  url:                 string;
  ssh_url:             string;
  http_url:            string;
}

interface Repository {
  name:        string;
  url:         string;
  description: string;
  homepage:    string;
}

interface User {
  id:         number;
  name:       string;
  username:   string;
  avatar_url: string;
  email:      string;
}
