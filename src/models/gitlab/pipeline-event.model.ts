// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#pipeline-events

// X-Gitlab-Event: Pipeline Hook

export interface PipelineEvent {
  object_kind:       string;
  object_attributes: ObjectAttributes;
  merge_request:     MergeRequest;
  user:              User;
  project:           PipelineEventProject;
  commit:            Commit;
  source_pipeline:   SourcePipeline;
  builds:            Build[];
}

interface Build {
  id:              number;
  stage:           string;
  name:            string;
  status:          string;
  created_at:      string;
  started_at:      null | string;
  finished_at:     null | string;
  duration:        number | null;
  queued_duration: number | null;
  failure_reason:  null | string;
  when:            string;
  manual:          boolean;
  allow_failure:   boolean;
  user:            User;
  runner:          Runner | null;
  artifacts_file:  ArtifactsFile;
  environment:     Environment | null;
}

interface ArtifactsFile {
  filename: null;
  size:     null;
}

interface Environment {
  name:            string;
  action:          string;
  deployment_tier: string;
}

interface Runner {
  id:          number;
  description: string;
  active:      boolean;
  runner_type: string;
  is_shared:   boolean;
  tags:        string[];
}

interface User {
  id:         number;
  name:       string;
  username:   string;
  avatar_url: string;
  email:      string;
}

interface Commit {
  id:        string;
  message:   string;
  timestamp: string;
  url:       string;
  author:    Author;
}

interface Author {
  name:  string;
  email: string;
}

interface MergeRequest {
  id:                number;
  iid:               number;
  title:             string;
  source_branch:     string;
  source_project_id: number;
  target_branch:     string;
  target_project_id: number;
  state:             string;
  merge_status:      string;
  url:               string;
}

interface ObjectAttributes {
  id:          number;
  ref:         string;
  tag:         boolean;
  sha:         string;
  before_sha:  string;
  source:      string;
  status:      string;
  stages:      string[];
  created_at:  string;
  finished_at: string;
  duration:    number;
  variables:   Variable[];
}

interface Variable {
  key:   string;
  value: string;
}

interface PipelineEventProject {
  id:                  number;
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
}

interface SourcePipeline {
  project:     SourcePipelineProject;
  pipeline_id: number;
  job_id:      number;
}

interface SourcePipelineProject {
  id:                  number;
  web_url:             string;
  path_with_namespace: string;
}

