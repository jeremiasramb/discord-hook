// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#subgroup-events

// X-Gitlab-Event: Subgroup Hook

export interface SubgroupEvent {
  created_at:       string;
  updated_at:       string;
  event_name:       string;
  name:             string;
  path:             string;
  full_path:        string;
  group_id:         number;
  parent_group_id:  number;
  parent_name:      string;
  parent_path:      string;
  parent_full_path: string;
}
