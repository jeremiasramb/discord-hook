// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#issue-events

// X-Gitlab-Event: Issue Hook

export interface IssueEvent {
  object_kind:       string;
  event_type:        string;
  user:              User;
  project:           Project;
  object_attributes: ObjectAttributes;
  repository:        Repository;
  assignees:         Assignee[];
  assignee:          Assignee;
  labels:            Label[];
  changes:           Changes;
}

interface Assignee {
  name:       string;
  username:   string;
  avatar_url: string;
}

interface Changes {
  updated_by_id: UpdatedByID;
  updated_at:    UpdatedAt;
  labels:        Labels;
}

interface Labels {
  previous: Label[];
  current:  Label[];
}

interface Label {
  id:          number;
  title:       string;
  color:       string;
  project_id:  number;
  created_at:  string;
  updated_at:  string;
  template:    boolean;
  description: string;
  type:        string;
  group_id:    number;
}

interface UpdatedAt {
  previous: string;
  current:  string;
}

interface UpdatedByID {
  previous: null;
  current:  number;
}

interface ObjectAttributes {
  id:                     number;
  title:                  string;
  assignee_ids:           number[];
  assignee_id:            number;
  author_id:              number;
  project_id:             number;
  created_at:             string;
  updated_at:             string;
  updated_by_id:          number;
  last_edited_at:         null;
  last_edited_by_id:      null;
  relative_position:      number;
  description:            string;
  milestone_id:           null;
  state_id:               number;
  confidential:           boolean;
  discussion_locked:      boolean;
  due_date:               null;
  moved_to_id:            null;
  duplicated_to_id:       null;
  time_estimate:          number;
  total_time_spent:       number;
  time_change:            number;
  human_total_time_spent: null;
  human_time_estimate:    null;
  human_time_change:      null;
  weight:                 null;
  iid:                    number;
  url:                    string;
  state:                  string;
  action:                 string;
  severity:               string;
  escalation_status:      string;
  escalation_policy:      EscalationPolicy;
  labels:                 Label[];
}

interface EscalationPolicy {
  id:   number;
  name: string;
}

interface Project {
  id:                  number;
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
  ci_config_path:      null;
  homepage:            string;
  url:                 string;
  ssh_url:             string;
  http_url:            string;
}

interface Repository {
  name:        string;
  url:         string;
  description: string;
  homepage:    string;
}

interface User {
  id:         number;
  name:       string;
  username:   string;
  avatar_url: string;
  email:      string;
}
