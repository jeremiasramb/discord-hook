// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#group-member-events

// X-Gitlab-Event: Member Hook

export interface GroupMemberEvent {
  created_at:    string;
  updated_at:    string;
  group_name:    string;
  group_path:    string;
  group_id:      number;
  user_username: string;
  user_name:     string;
  user_email:    string;
  user_id:       number;
  group_access:  string;
  group_plan:    null;
  expires_at:    string;
  event_name:    string;
}
