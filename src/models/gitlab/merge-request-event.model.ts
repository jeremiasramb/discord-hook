// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#merge-request-events

// X-Gitlab-Event: Merge Request Hook

export interface MergeRequestEvent {
  object_kind:       string;
  event_type:        string;
  user:              User;
  project:           Project;
  repository:        Repository;
  object_attributes: ObjectAttributes;
  labels:            Label[];
  changes:           Changes;
  assignees:         User[];
  reviewers:         User[];
}

interface User {
  id:         number;
  name:       string;
  username:   string;
  avatar_url: string;
  email?:     string;
}

interface Changes {
  updated_by_id: UpdatedByID;
  updated_at:    UpdatedAt;
  labels:        Labels;
}

interface Labels {
  previous: Label[];
  current:  Label[];
}

interface Label {
  id:          number;
  title:       string;
  color:       string;
  project_id:  number;
  created_at:  string;
  updated_at:  string;
  template:    boolean;
  description: string;
  type:        string;
  group_id:    number;
}

interface UpdatedAt {
  previous: string;
  current:  string;
}

interface UpdatedByID {
  previous: null;
  current:  number;
}

interface ObjectAttributes {
  id:                            number;
  iid:                           number;
  target_branch:                 string;
  source_branch:                 string;
  source_project_id:             number;
  author_id:                     number;
  assignee_ids:                  number[];
  assignee_id:                   number;
  reviewer_ids:                  number[];
  title:                         string;
  created_at:                    string;
  updated_at:                    string;
  milestone_id:                  null;
  state:                         string;
  blocking_discussions_resolved: boolean;
  work_in_progress:              boolean;
  first_contribution:            boolean;
  merge_status:                  string;
  target_project_id:             number;
  description:                   string;
  url:                           string;
  source:                        Project;
  target:                        Project;
  last_commit:                   LastCommit;
  labels:                        Label[];
  action:                        string;
}

interface LastCommit {
  id:        string;
  message:   string;
  timestamp: string;
  url:       string;
  author:    Author;
}

interface Author {
  name:  string;
  email: string;
}

interface Project {
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
  homepage:            string;
  url:                 string;
  ssh_url:             string;
  http_url:            string;
  id?:                 number;
}

interface Repository {
  name:        string;
  url:         string;
  description: string;
  homepage:    string;
}
