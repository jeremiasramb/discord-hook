// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#wiki-page-events

// X-Gitlab-Event: Wiki Page Hook

export interface WikiPageEvent {
  object_kind:       string;
  user:              User;
  project:           Project;
  wiki:              Wiki;
  object_attributes: ObjectAttributes;
}

interface ObjectAttributes {
  title:   string;
  content: string;
  format:  string;
  message: string;
  slug:    string;
  url:     string;
  action:  string;
}

interface Project {
  id:                  number;
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
  homepage:            string;
  url:                 string;
  ssh_url:             string;
  http_url:            string;
}

interface User {
  id:         number;
  name:       string;
  username:   string;
  avatar_url: string;
  email:      string;
}

interface Wiki {
  web_url:             string;
  git_ssh_url:         string;
  git_http_url:        string;
  path_with_namespace: string;
  default_branch:      string;
}
