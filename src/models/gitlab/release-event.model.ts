// https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#release-events

// X-Gitlab-Event: Release Hook

export interface ReleaseEvent {
  id:          number;
  created_at:  string;
  description: string;
  name:        string;
  released_at: string;
  tag:         string;
  object_kind: string;
  project:     Project;
  url:         string;
  action:      string;
  assets:      Assets;
  commit:      Commit;
}

interface Assets {
  count:   number;
  links:   Link[];
  sources: Source[];
}

interface Link {
  id:        number;
  external:  boolean;
  link_type: string;
  name:      string;
  url:       string;
}

interface Source {
  format: string;
  url:    string;
}

interface Commit {
  id:        string;
  message:   string;
  title:     string;
  timestamp: string;
  url:       string;
  author:    Author;
}

interface Author {
  name:  string;
  email: string;
}

interface Project {
  id:                  number;
  name:                string;
  description:         string;
  web_url:             string;
  avatar_url:          null;
  git_ssh_url:         string;
  git_http_url:        string;
  namespace:           string;
  visibility_level:    number;
  path_with_namespace: string;
  default_branch:      string;
  ci_config_path:      null;
  homepage:            string;
  url:                 string;
  ssh_url:             string;
  http_url:            string;
}
