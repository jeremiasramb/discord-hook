export interface GitlabMergeRequestDiscordHook {
  project: string
  projectName: string
  channelId: bigint
  discordToken: string
  gitlabToken: string
}
