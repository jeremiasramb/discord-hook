import { Request as IRequest } from 'itty-router'
export type IttyRequest = Request & IRequest
