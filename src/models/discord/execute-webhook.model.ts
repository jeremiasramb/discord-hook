export interface ExecuteWebhook {
  /** the message contents (up to 2000 characters) */
  content?: string
  /** override the default username of the webhook */
  username?: string
  /** override the default avatar of the webhook */
  avatar_url?: string
  /** true if this is a TTS message */
  tts?: boolean
  /** embedded rich content */
  embeds?: Embed[]
  /** allowed mentions for the message */
  allowed_mentions?: any
  /** the components to include with the message */
  components?: any
  /** the contents of the file being sent */
  files?: any
  /** JSON encoded body of non-file params */
  payload_json?: any
  /** attachment objects with filename and description */
  attachments?: any
  /** message flags combined as a bitfield (only SUPPRESS_EMBEDS can be set) */
  flags?: number
  /** name of thread to create (requires the webhook channel to be a forum channel) */
  thread_name?: string
}

interface Embed {
  /** title of embed */
  title?: string
  /** type of embed (always "rich" for webhook embeds) */
  type?: EmbedTypes
  /** description of embed */
  description?: string
  /** url of embed */
  url?: string
  /** timestamp of embed content */
  timestamp?: Date | string
  /** color code of the embed */
  color?: number
  /** footer information */
  footer?: EmbedFooter
  /** image information */
  image?: EmbedImage
  /** thumbnail information */
  thumbnail?: EmbedThumbnail
  /** video information */
  video?: EmbedVideo
  /** provider information */
  provider?: EmbedProvider
  /** author information */
  author?: EmbedAuthor
  /** fields information */
  fields?: EmbedField[]
}

enum EmbedTypes {
  /** generic embed rendered from embed attributes */
  rich,
  /** image embed */
  image,
  /** video embed */
  video,
  /** animated gif image embed rendered as a video embed */
  gifv,
  /** article embed */
  article,
  /** link embed */
  link
}

interface EmbedFooter {
  /** footer text */
  text: string
  /** url of footer icon (only supports http(s) and attachments) */
  icon_url?: string
  /** a proxied url of footer icon */
  proxy_icon_url?: string
}

interface EmbedImage {
  /** source url of image (only supports http(s) and attachments) */
  url?: string
  /** a proxied url of the image */
  proxy_url?: string
  /** height of image */
  height?: number
  /** width of image */
  width?: number
}

interface EmbedThumbnail {
  /** source url of thumbnail (only supports http(s) and attachments) */
  url: string
  /** a proxied url of the thumbnail */
  proxy_url?: string
  /** height of thumbnail */
  height?: number
  /** width of thumbnail */
  width?: number
}

interface EmbedVideo {
  /** source url of video */
  url?: string
  /** a proxied url of the video */
  proxy_url?: string
  /** height of video */
  height?: number
  /** width of video */
  width?: number
}

interface EmbedProvider {
  /** name of provider */
  name?: string
  /** url of provider */
  url?: string
}

interface EmbedAuthor {
  /** name of author */
  name: string
  /** url of author */
  url?: string
  /** url of author icon (only supports http(s) and attachments) */
  icon_url?: string
  /** a proxied url of author icon */
  proxy_icon_url?: string
}

interface EmbedField {
  /** name of the field */
  name: string
  /** value of the field */
  value: string
  /** whether or not this field should display inline */
  inline?: boolean
}

export interface ExecuteWebhookProps {
  id: bigint
  token: string
}
